# Getting Started

This project was create with Socket.io.

## Available Scripts

First we clone repo to localhost then install packages, you can run:

### `yarn`

Or

### `npm i`



In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
URL is [http://localhost:8080](http://localhost:8080) that we create socket server.

