const express = require('express');
const cors = require('cors');
require('dotenv').config()
const fs = require('fs')
const PORT = process.env.PORT || 8080

const events = {
    mapCamera: "MAP_CAMERA",
    mapZoom: "MAP_ZOOM",
    mapTerrain: "MAP_TERRAIN",
    send_mapCamera: "SEND_MAP_CAMERA",
    send_mapZoom: "SEND_MAP_ZOOM",
    send_mapTerrain: "SEND_MAP_TERRAIN",
    sync: "SYNC",
    send_sync: "SEND_SYNC",
    onClickMarker: "ON_CLICK_MARKER",
    send_onClickMarker: "SEND_ON_CLICK_MARKER",
    openDataDetail: "OPEN_DATA_DETAIL",
    send_openDataDetail: "SEND_OPEN_DATA_DETAIL",
    changeDateXRangeSlider: "CHANGE_DATE_XRANGE_SLIDER",
    send_changeDateXRangeSlider: "SEND_CHANGE_DATE_XRANGE_SLIDER",
    changeDateRange: "CHANGE_DATE_RANGE",
    send_changeDateRange: "SEND_CHANGE_DATE_RANGE",
    selectDataLayer: "SELECT_DATA_LAYER",
    send_selectDataLayer: "SEND_SELECT_DATA_LAYER",
    textureSelected: "TEXTURE_SELECTED",
    send_textureSelected: "SEND_TEXTURE_SELECTED",
    countrySelected: "COUNTRY_SELECTED",
    send_countrySelected: "SEND_COUNTRY_SELECTED",
    beaconSelected: "BEACON_SELECTED",
    send_beaconSelected: "SEND_BEACON_SELECTED",
    sortedTexture:"SORTED_TEXTURE",
    send_sortedTexture: "SEND_SORTED_TEXTURE",
    pulseMarker: "PULSE_MARKER",
    send_pulseMarker: "SEND_PULSE_MARKER",
};

const siteProfiles = ['GLOBAL', 'TH', 'VN', 'MM', 'KH']

var app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.get('/', (req, res, next) => {
    res.send(200);
})

var httpsServer
console.log('PRODUCTION',process.env.PRODUCTION);
if (process.env.PRODUCTION === 'true') {
    const domainName = process.env.SERVER_URL.replace('https://', '')
    const privateKey = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/privkey.pem`, 'utf8') || '';
    const certificate = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/cert.pem`, 'utf8') || '';
    const ca = fs.readFileSync(`/etc/letsencrypt/live/${domainName}/chain.pem`, 'utf8') || '';
    const credentials = {
        key: privateKey,
        cert: certificate,
        ca: ca
    };
    httpsServer = require('https').createServer(credentials, app)
    httpsServer.listen(PORT, function () {
        console.log('https server running on port ' + PORT + '.');
    });

} else {
    httpsServer = require('http').createServer(app)
    httpsServer.listen(PORT, function () {
        console.log('http server running on port ' + PORT + '.');
    });
}

const io = require('socket.io')(httpsServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});
io.on('connection', function (socket) {
    socket.join(siteProfiles[0]);
    socket.room = siteProfiles[0];
    socket.on('JOIN', function (data) {
        console.log(`JOIN:`, data);
        socket.leave(socket.room);
        socket.join(data);
        socket.room = data;
    });

    Object
        .keys(events)
        .filter(item => item.indexOf('send_') < 0)
        .forEach(item => {
            socket
                .on(events[item], function (data) {
                    console.log(`${events[item]}:`, data);
                    socket
                        .to(socket.room)
                        .emit(events[`send_${item}`], data);
                });
        });
});
